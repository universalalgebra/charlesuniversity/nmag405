# NMAG 405 Practical 5

5 Nov 2020

------------------------------------------

**Contents**

- [Topics](#topics)
    - [References](#references)
- [Sg](#sg)
- [Homomorphisms](#homomorphisms)
    - [Exercise: Find all homomorphisms from 𝑨 to 𝑩](#exercise-find-all-homomorphisms-from-𝑨-to-𝑩)
    - [Homomorphisms are determined by their action on generating sets](#homomorphisms-are-determined-by-their-action-on-generating-sets)
- [H, S, P operators](#h-s-p-operators)
- [SH ≤ HS, PS ≤ SP, PH ≤ HP](#sh--hs-ps--sp-ph--hp)
    - [SH ≤ HS](#sh--hs)
    - [PS ≤ SP](#ps--sp)
    - [PH ≤ HP](#ph--hp)
    - [Exercise: SH ≠ HS, PS ≠ SP, PH ≠ HP](#exercise-sh--hs-ps--sp-ph--hp)
- [Equational classes](#equational-classes)
    - [𝕍 = HSP](#𝕍--hsp)
- [Locally finite varieties](#locally-finite-varieties)
    - [Examples](#examples)
- [Theorem: Every finitely generated variety is locally finite](#theorem-every-finitely-generated-variety-is-locally-finite)
    - [Proof](#proof)
- [Exercise: the equalizer is a subuniverse](#exercise-the-equalizer-is-a-subuniverse)

-------------------------------------

## Topics

1. Homomorphism

2. Varieties and the closure operators H, S, P

3. Finite algebras generate locally finite varietie

### References

+ Bergman 1.1, 1.3, 3.1, 3.5
+ [Lecture notes](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_05.pdf)

-------------------------------------

## Sg

For an algebra 𝑨 = (𝐴, 𝐹) and a subset 𝑋 ⊆ 𝐴, define Sgᴬ(X) by induction as follows:

+ ∀ x ∈ X → x ∈ Sgᴬ(X)

+ ∀ 𝑓 ∈ 𝐹, ∀ 𝒂 ∈ 𝐴ⁿ (where arity of 𝑓 is 𝑛),

  if ∀ 𝑖, 𝒂(𝑖) ∈ Sgᴬ(X), then 

   𝑓ᴬ(𝒂) ∈ Sgᴬ(X)

-------------------------------------

## Homomorphisms

### Exercise: Find all homomorphisms from 𝑨 to 𝑩

Let ℕ = {0, 1, …} and 𝑨 = (ℕ, +) × (ℕ, +) and 𝑩 = ({-1, 1}, ·).

Find all homomorphisms from 𝑨 to 𝑩.


### Homomorphisms are determined by their action on generating sets

(Exercise 1.16.6 of Bergman)

#### If 𝑋 is a set, then each ℎ ∈ Hom(Sg(X), 𝑩) is determined by it values on 𝑋

Let ℎ, 𝑔 ∈ Hom(Sgᴬ(𝑋), 𝑩).

To show: ∀ 𝑥 ∈ 𝑋, ℎ(𝑥) = 𝑔(𝑥) implies ℎ = 𝑔, 

Fix 𝑎 ∈ Sgᴬ(𝑋).  We must show ℎ(𝑎) = 𝑔(𝑎).

+ *Base step*. If 𝑎 ∈ 𝑋, then ℎ(𝑎) = 𝑔(𝑎) by assumption.

+ *Induction step*. Assume 𝑎 = 𝑓ᴬ(𝒃) for some 𝒃 ∈ 𝐴ⁿ, and assume (the induction hypothesis) 

  ∀ 𝑖, ℎ(𝒃 𝑖) = 𝑔(𝒃 𝑖)  (IH)

  Then ℎ(𝑎) = 𝑔(𝑎). Indeed, by IH,
  
  ℎ(𝑎) = ℎ(𝑓ᴬ(𝒃)) = 𝑓ᴮ(ℎ(𝒃(1)), …, ℎ(𝒃(𝑛))) = 𝑓ᴮ(𝑔(𝒃(1)), …, 𝑔(𝒃(𝑛))) = 𝑔(𝒂).

---------------------------------------

#### In general, not every map extends to a homomorphism

Let 𝑨, 𝑩 be algebras of the same signature and suppose 𝑋 ⊆ 𝐴 generates 𝑨.

Let ℎ : 𝑋 → 𝐵 be an arbitrary map.

It is not always possible to find a homomorphism 𝑨 → 𝑩 that agrees with ℎ on 𝑋.

An easy way to see this is to consider the extreme case in which 𝑋 = 𝐴.

Then the claim that arbitrary maps extend to a homomorphism is equivalent to 
the claim that *every* map from 𝐴 to 𝐵 is a homomorphism---obviously false in general.

---------------------------------------

## H, S, P operators

Fix a signature 𝑆. Let 𝒦 be a class of 𝑆-algebras. Define

+ H(𝒦) = homomorphic images of members of 𝒦
+ S(𝒦) = algebras isomorphic to a subalgebra of a member of 𝒦
+ P(𝒦) = algebras isomorphic to a direct product of members of 𝒦

A class 𝒦 of 𝑆-algebras is said to be **closed under homomorphic images** if H(𝒦) ⊆ 𝒦.

Similarly, 𝒦 is closed under **subalgebras** (resp. **products**) if S(𝒦) ⊆ 𝒦 (resp. P(𝒦) ⊆ 𝒦).

An algebra is a homomorphic image (or subalgebra or product) of every algebra to which it is isomorphic. 

Thus, the classes H(𝒦), S(𝒦), and P(𝒦) are closed under isomorphism.

The operators H, S, P can be composed with one another repeatedly, forming yet more closure operators.

If C₁ and C₂ are closure operators on classes of structures, let us say that C₁ ≤ C₂ if for every class 𝒦 we have C₁(𝒦) ⊆ C₂(𝒦).

### Exercise: each of H, S, and P is a closure operator.

----------------------------------------------------------------

## SH ≤ HS, PS ≤ SP, PH ≤ HP

### SH ≤ HS

Let 𝑪 ∈ SH(𝒦). Then 𝑪 ≤ 𝑩 for some 𝑩 ∈ H(𝑨) for some 𝑨 ∈ 𝒦.

If θ is such that 𝑩 ≅ 𝑨/θ, then 𝑪 is isomorphic to a subalgebra (say, 𝑻) of 𝑨/θ.

By the correspondence theorem, there is a subalgebra 𝑺 ≤ 𝑨 such that 𝑺/θ = 𝑻.

Thus, 𝑪 ∈ HS(𝑨) ⊆ HS(𝒦), as desired.

----------------------------------------------------------------

### PS ≤ SP

Let 𝑪 ∈ PS(𝒦). Then 𝑪 = Π 𝑩ᵢ for some 𝑩ᵢ ≤ 𝑨ᵢ ∈ 𝒦.

Clearly, 𝑪 = Π 𝑩ᵢ ≤ Π 𝑨ᵢ, so 𝑪 ∈ SP(𝒦), as desired. 

----------------------------------------------------------------

### PH ≤ HP

Let 𝑪 ∈ PH(𝒦).  Then 𝑪 = Π (𝑨ᵢ/θᵢ) for some 𝑨ᵢ ∈ 𝒦 and θᵢ ∈ Con(𝑨ᵢ).

We claim that Π (𝑨ᵢ/θᵢ) ≅ (Π 𝑨ᵢ)/Θ, where Θ = { (𝒂, 𝒃) ∈ (Π 𝑨ᵢ)² : ∀ i, (𝒂 i, 𝒃 i) ∈ θᵢ } is a congruence of Π 𝑨ᵢ.
   
That is, we claim
   
a. Θ ∈ Con((Π 𝑨ᵢ)²)
b. ∃ homs f : 𝑪 → (Π 𝑨ᵢ)/Θ and g : (Π 𝑨ᵢ)/Θ → 𝑪 such that 
   + f ∘ g is the identity on (Π 𝑨ᵢ)/Θ
   + g ∘ f is the identity on 𝑪

*Proof of a*. Θ is obviously an equivalence, so it suffices to show it is compatible with the operations of Π 𝑨ᵢ.
   
Let 𝑓 be an operation symbol of 𝑆. Assume 𝑓 is 𝑛-ary.  For 1 ≤ 𝑗 ≤ 𝑛, let {𝒂ⱼ}, {𝒃ⱼ} ⊆ Π 𝐴ᵢ be such that for all 𝑖, 𝑗,
   
(𝒂ⱼ i, 𝒃ⱼ i) ∈ θᵢ.  Then 
   
𝒂' := 𝑓<sup>Π 𝑨ᵢ</sup> (𝒂₁, …, 𝒂ₙ)  = (𝑓<sup>𝑨₁</sup>(𝒂₁ 1, …, 𝒂ₙ 1), 𝑓<sup>𝑨₂</sup>(𝒂₁ 2, …, 𝒂ₙ 2), … )

and
   
𝒃' := 𝑓<sup>Π 𝑨ᵢ</sup> (𝒃₁, …, 𝒃ₙ) = (𝑓<sup>𝑨₁</sup> (𝒃₁ 1, …, 𝒃ₙ 1), 𝑓<sup>𝑨₂</sup>(𝒃₁ 2, …, 𝒃ₙ 2), … )
   
and for each 𝑗, 

(𝑓<sup>𝑨ⱼ</sup>(𝒂₁ 𝑗, …, 𝒂ₙ 𝑗), 𝑓<sup>𝑨ⱼ</sup>(𝒃₁ 𝑗, …, 𝒃ₙ 𝑗)) ∈ θⱼ

Therefore, (𝒂', 𝒃') ∈ Θ, as desired.

*Prove of b*. Let f be the natural map that takes a tuple ⟦ 𝒂 ⟧ := (𝑎₁/θ₁, 𝑎₂/θ₂, … ) of 𝑪 = Π (𝑨ᵢ/θᵢ) to the class of (Π 𝑨ᵢ)/Θ that contains the tuple (𝑎₁, 𝑎₂, … ) of representatives of the entries of ⟦ 𝒂 ⟧. Let g be the map that takes a class (𝑎₁, 𝑎₂, …)/Θ to the tuple (𝑎₁/θ₁, 𝑎₂/θ₂, …) ∈ 𝑪.  It should be clear that f and g compose to the respective identity maps, and checking that f and g are homomorphisms is a straight-forward verification.

----------------------------------------------------------------

### Exercise: SH ≠ HS, PS ≠ SP, PH ≠ HP

-------------------------------------------------------------------

## Equational classes

A class 𝒦 of 𝑆-algebras is called a **variety** if it is closed under each of the closure operators H, S, and P; the corresponding closure operator is denoted 𝕍.

Thus, if 𝒦 is a class of similar algebras, then the **variety generated by** 𝒦 is denoted by 𝕍(𝒦) and defined to be the smallest class that contains 𝒦 and is closed under H, S, and P.

The class of all varieties of 𝑆-algebras is ordered by inclusion, and closed under arbitrary intersection; thus, the class of varieties is a complete lattice.

We would like to know how to construct 𝕍(𝒦) directly from 𝒦, but it's not immediately obvious how many times we would have to apply the operators H, S, P before the result stabilizes to form a variety---the **variety generated by** 𝒦.

Fortunately, it was proved (by Birkhoff and/or Tarski (ref needed)) that if we apply the operators in the correct order, then it suffices to apply each only once.

### 𝕍 = HSP

**Theorem** (Thm 3.43 of Bergman) 𝕍 = HSP.

*Proof*. Let 𝒦 be a class of algebras. To see that HSP(𝒦) is a variety, we use the above inclusions to compute H(HSP) = HSP, S(HSP) ≤ HS²P = HSP, P(HSP) ≤ HSP² = HSP. Thus HSP ≥ 𝕍. On the other hand, HSP(𝒦) ⊆ HSP(𝕍(𝒦)) = 𝕍(𝒦) so HSP ≤ 𝕍.

-------------------------------------------------------------------------------------

## Locally finite varieties

**Definition** (Def 3.48 of Bergman)

1. An algebra is called **locally finite** if every finitely generated subalgebra is finite. 
   A variety is **locally finite** if every member is locally finite.
2. A variety is called **finitely generated** if it is of the form 𝕍(𝒦** in which 𝒦 is a 
    finite set of finite algebras.

**Remark** Part 2 of the definition may seem odd if you expected "finitely generated variety" to be a variety generated by a finite number of algebras. However, *every* variety can be generated by a single algebra, so such a definition would be meaningless.
    
---------------------------------

### Examples

#### The Abelian group ℤ is not locally finite

For any 𝑛 ≠ 0, the subgroup ⟨ 𝑛 ⟩ generated by 𝑛 is infinite. 

#### The groups ℤ(𝑝<sup>∞</sup>) are locally finite. 

This follows from Exercise 3.26.7 of Bergman.

#### The variety of distributive lattice is locally finite.

See Exercise 2.47.7 of Bergman.

---------------------------------------------

## Theorem: Every finitely generated variety is locally finite

(Theorem 3.49 of Bergman) 

### Proof

Let 𝒱 be a finitely generated variety. Thus 𝒱 = 𝕍(𝒦) where 𝒦 = {𝑨₁, 𝑨₂, …, 𝑨ₙ}, 𝑛 is a natural number and each 𝑨ₖ is finite. 

Let 𝑩 be a finitely generated member of 𝒱, say 𝑩 is generated by the finite set 𝑌.  We must show 𝑩 is finite.

#### Claim 1. ∃ 𝑪 ∈ SP(𝒦) such that ℎ : 𝑪 ↠ 𝑩, for some epimorphism ℎ

By 𝑩 ∈ 𝕍(𝒦) = HSP(𝒦) (Thm 3.43).

#### Claim 2. ∃ 𝑪ᵢ ∈ 𝒦 (𝑖 ∈ 𝐼) such that 𝑪 ↣ Π 𝑪ᵢ

Since 𝑪 ∈ SP(𝒦).

Since ℎ is onto, it suffices to show 𝑪 is finite.

#### Claim 3. 𝑪 is finite

(Keep in mind, each algebra in 𝒦, hence each 𝑪ᵢ, is finite!  
So if Π 𝑪ᵢ has finitely many terms, then it, hence 𝑪, is finite.  
We show that wlog Π 𝑪ᵢ is in fact a product of finitely many terms.)

##### Subclaim 3.1. We can assume 𝑪 is finitely generated.

Reason: Since ℎ is onto, there is a subset 𝑋 ⊆ 𝐶 such that ℎ|ₓ is a bijection from 𝑋 to 𝑌;
since 𝑌 generates 𝑩, if we restrict ℎ to the subalgebra Sgᶜ(𝑋) of 𝑪, then we still have 
an epimorphism from 𝑪':= Sgᶜ(𝑋) onto 𝑩.

So, from now on, we assume 𝑪 is the *finitely generated* algebra Sgᶜ(𝑋).

##### Subclaim 3.2 For each 𝑗, Hom(𝑪, 𝑪ⱼ) is finite.

Since 𝑪 is finitely generated, Hom(𝑪, 𝑨ₖ) is a finite set. (Exercise 1.16.6, solved above)


Let 𝑝ⱼ : Π 𝐶ᵢ → 𝐶ⱼ denote the projection of Π 𝐶ᵢ onto the 𝑗th factor.

Let 𝑔 : 𝑪 ↣ Π 𝑪ᵢ and, ∀ 𝑖 ∈ 𝐼, 

##### Subclaim 3.3 The set {𝑝ᵢ ∘ 𝑔 : 𝑖 ∈ 𝐼} ⊆ ⋃ Hom(𝑪, 𝑨ₖ) is finite

Since {𝑝ᵢ ∘ 𝑔 : 𝑖 ∈ 𝐼} ⊆ ⋃ Hom(𝑪, 𝑨ₖ) there are only finitely many maps 𝑝ᵢ ∘ 𝑔.

Thus there is a finite subset 𝐽 ⊆ 𝐼 such that

{𝑝ᵢ ∘ 𝑔 : 𝑖 ∈ 𝐼} = {𝑝ⱼ ∘ 𝑔 : 𝑗 ∈ 𝐽}.

Therefore,

  0 = ⋂ {ker (𝑝ᵢ ∘ 𝑔) : 𝑖 ∈ 𝐼} = ⋂ {ker (𝑝𝑗 ∘ 𝑔) : 𝑗 ∈ 𝐽}, 
  
so 𝑪 embeds in the product Πⱼ 𝑪ⱼ.

##### Subclaim 3.4. 𝑪 is finite

Since this is a finite product of finite algebras, we conclude that 𝐶 is finite.

□

---------------------------------------------

## Exercise: the equalizer is a subuniverse

Let 𝑓 and 𝑔 be homomorphisms from 𝑨 to 𝑩.

Let E(𝑓,𝑔) = {𝑎 ∈ 𝐴 : 𝑓(𝑎) = 𝑔(𝑎)}, the **equalizer** 𝑓 and 𝑔. 

Prove that E(𝑓, 𝑔) is a subuniverse of 𝑨.
