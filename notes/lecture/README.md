## Lecture notes

### Schedule of lectures, topics, and homework

date | topics (future topics may change)	| recommended reading | lecture notes |	homework
--- | --- | --- | --- | --- 
8.10 |	Motivation. Algebra (signature, type). Examples. Pr.: Lattices vs. lattice ordered sets	| Bergman 1.1, 1.2 | [lecture 1](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_01.pdf) | 
15.10 |	Lattices, complete lattices, closure operators. Pr.: Distributive and modular lattices. The lattice of equivalence relations. | Bergman 2.1, 2.2, 2.3 | [lecture 2](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_02.pdf) | 
22.10 | Algebraic lattices and closure operators. Galois correspondences. Pr.: Complete lattices, closure operators, Galois correspondences. | Bergman 2.4, 2.5 | [lecture 3](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_03.pdf) | [Homework 1](https://www2.karlin.mff.cuni.cz/~barto/UA/UA2021Z_HWK1.pdf) (due 5 Nov)
29.10 | Subalgebras, products, quotients. Pr.: Subalgebras, congruences. | Bergman 1.3, 1.4, 1.5 | [lecture 4](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_04.pdf) [corrected](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_04_corrected.pdf) | 
5.11 | H,S,P operators, variety. Homomorphisms. Pr.: Homomorphisms. Finite algebras generate locally finite varieties. | Bergman 1.1, 1.3, 3.1, 3.5 | [lecture 5](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_05.pdf) | [Homework 2](https://www2.karlin.mff.cuni.cz/~barto/UA/UA2021Z_HWK2.pdf) (due 19 Nov)
12.11 | Direct and subdirect decomposition. Pr.: Direct and subdirect decomposition. | Bergman 3.2, 3.3 | [lecture 6](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_06.pdf) |
19.11 | Subdirect decomoposition, SIs in congruence distributive vaieties. Pr.: SIs in monounary algebras. | Bergman 3.4, 3.5, (5.2) | [lecture 7](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_07.pdf) | [Homework 3](https://www2.karlin.mff.cuni.cz/~barto/UA/UA2021Z_HWK3.pdf) (due 3 Dec)
26.11 | Terms, identities, free algebras. Pr.: Free algebras. | Bergman 4.3, 4.4 | [lecture 8](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_08.pdf) | 
3.12 | The syntax-semantics Galois correspondence, Birkhoff's theorem. Pr.: Equational bases. | Bergman 4.4, (4.6) | [lecture 9](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_09.pdf) | [Homework 4](https://www2.karlin.mff.cuni.cz/~barto/UA/UA2021Z_HWK4.pdf) (due 17 Dec)
10.12 | Clones. Free algebras as clones of term operations. Pr.: Clones. | Bergman 4.1 | [lecture 10](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_10.pdf) |
17.12 | The operations-relations Galois correspondence. Pr.: Algebraic and relational clones. | Bergman 4.2 | [lecture 11](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_11.pdf) | Homework 5 (due 7 Jan)
7.1 | Mal'cev conditions: Mal'cev, majority. Mal'cev conditions | Bergman 4.7 (part) | [lecture 12](https://www2.karlin.mff.cuni.cz/~barto/UA/UA_Lecture_Notes_12.pdf) |
